#ifndef GAME_HPP
#define GAME_HPP

#include <iostream>
#include <memory>
#include <stack>

#include <SFML/Graphics.hpp>

#include "GameState.hpp"
#include "Player.hpp"
#include "SettingsParser.hpp"
#include "TileMap.hpp"

class GameState;
class Player;


class Game
{
private:

  void loadSettings();
  
public:

  const static int tileSize = 32;

  std::stack<std::shared_ptr<GameState>> states;

  sf::RenderWindow window;
  Player player;

  int windowWidth;
  int windowHeight;
  bool fullscreen;

  int soundVolume;
  int musicVolume;

  int levelWidth;
  int levelHeight;

  sf::View view;
  sf::View miniMapView;

  std::vector<int> level;

  TileMap map;
  TileMap miniMap;

  sf::Font font;
  sf::Text debugText;
	
  void pushState(std::shared_ptr<GameState> state);
  void popState();
  void changeState(std::shared_ptr<GameState> state);
  std::shared_ptr<GameState> peekState();

  void gameLoop();

  Game();
  ~Game();
};

#endif // GAME_HPP
