#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "Game.hpp"

class Game;

class GameState
{
    public:

    Game* game;

    virtual void draw(const float dt) = 0;
    virtual void update(const float dt) = 0;
};

#endif // GAMESTATE_HPP
