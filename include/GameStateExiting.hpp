#ifndef GAMESTATEEXITING_HPP
#define GAMESTATEEXITING_HPP

#include "GameState.hpp"

class Game;

class GameStateExiting : public GameState
{
public:
  void move(const float dt);
  void update(const float dt);
  void draw(const float dt);

  GameStateExiting(Game* game);
  
private:

};

#endif // GAMESTATEEXITING_HPP
