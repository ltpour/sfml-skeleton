#ifndef GAMESTATERUNNING_HPP
#define GAMESTATERUNNING_HPP

#include "GameState.hpp"

class Game;

class GameStateRunning : public GameState
{
public:
  bool playerHorizontallyWithinTile();
  bool playerVerticallyWithinTile();
  void move(const float dt);
  void update(const float dt);
  void draw(const float dt);

  GameStateRunning(Game* game);
  
private:

};

#endif // GAMESTATERUNNING_HPP
