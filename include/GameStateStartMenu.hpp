#ifndef GAMESTATESTARTMENU_HPP
#define GAMESTATESTARTMENU_HPP

#include "GameState.hpp"

class Game;

class GameStateStartMenu : public GameState
{
public:
  void move(const float dt);
  void update(const float dt);
  void draw(const float dt);

  GameStateStartMenu(Game* game);
  
private:

};

#endif // GAMESTATESTARTMENU_HPP
