#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SFML/Graphics.hpp>

enum directions_t {STILL, NORTH, SOUTH, EAST, WEST};

class Player {
  
private:
  
public:

  sf::CircleShape shape;
  float x, y;
  directions_t direction;
  
  void move (float offsetX, float offsetY);
  const sf::Vector2f& getPosition () const;
  void setPosition (float x, float y);
  void lockHorizontallyToGrid (int tileSize);
  void lockVerticallyToGrid (int tileSize);
  
  Player();
  ~Player();
  
};

#endif // PLAYER_HPP
