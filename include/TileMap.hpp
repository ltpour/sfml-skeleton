////////////////////////////////////////////////////////////
// The code in this file originally from SFML tutorials,
// www.sfml-dev.org/tutorials/2.4/graphics-vertex-array.php
////////////////////////////////////////////////////////////

#ifndef TILEMAP_H
#define TILEMAP_H

#include <SFML/Graphics.hpp>

class TileMap : public sf::Drawable, public sf::Transformable
{
public:

  bool load(const std::string& tileset, sf::Vector2u tileSize, const std::vector<int> &tiles, unsigned int width, unsigned int height);

  bool load(const std::string& tileset, sf::Vector2u tileSize, const std::vector<int> &tiles, unsigned int width, unsigned int height, sf::Color color);

private:

  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
  {
    // apply the transform
    states.transform *= getTransform();

    // apply the tileset texture
    states.texture = &m_tileset;

    // draw the vertex array
    target.draw(m_vertices, states);
  }
    
  sf::VertexArray m_vertices;
  sf::Texture m_tileset;
};

#endif // TILEMAP_H
