#include <iostream>
#include <memory>
#include <stack>

#include <SFML/Graphics.hpp>

#include "Game.hpp"
#include "GameState.hpp"
#include "Player.hpp"
#include "SettingsParser.hpp"
#include "TileMap.hpp"

void Game::loadSettings()
{
  SettingsParser settings;
  if(!settings.loadFromFile("settings.txt"))
    {
      std::cout << "Error loading settings!" << std::endl; // SHOULD THROW EXCEPTION
    }

  // graphics settings
  windowWidth = 800;
  windowHeight = 600;
  fullscreen = false;
  settings.get("width", windowWidth);
  settings.get("height", windowHeight);
  settings.get("fullscreen", fullscreen);
  
  // audio settings
  soundVolume = 75;
  musicVolume = 75;
  settings.get("sound", soundVolume);
  settings.get("music", musicVolume);

  // create window
  this->window.create(sf::VideoMode(windowWidth, windowHeight, sf::VideoMode().getDesktopMode().bitsPerPixel), "SFML Skeleton", fullscreen ? sf::Style::Fullscreen : sf::Style::None);
  
  this->window.setFramerateLimit(60);

  // tilemap
  levelWidth = 32;
  levelHeight = 24;

  level = 
    {
      1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
      1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 
      2, 2, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 
      1, 2, 1, 1, 3, 1, 4, 4, 4, 1, 2, 2, 2, 1, 1, 1, 1, 2, 1, 1, 3, 1, 4, 4, 4, 1, 2, 2, 2, 1, 1, 1, 
      1, 2, 2, 1, 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 1, 1, 1, 2, 2, 1, 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 1, 1, 
      1, 1, 2, 1, 4, 1, 3, 3, 1, 1, 2, 2, 2, 2, 3, 1, 1, 1, 2, 1, 4, 1, 3, 3, 1, 1, 2, 2, 2, 2, 3, 1,  
      3, 1, 2, 1, 4, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 3, 1, 2, 1, 4, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 
      1, 1, 2, 1, 4, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 2, 1, 4, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2,
      1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
      1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 
      2, 2, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 
      1, 2, 1, 1, 3, 1, 4, 4, 4, 1, 2, 2, 2, 1, 1, 1, 1, 2, 1, 1, 3, 1, 4, 4, 4, 1, 2, 2, 2, 1, 1, 1, 
      1, 2, 2, 1, 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 1, 1, 1, 2, 2, 1, 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 1, 1, 
      1, 1, 2, 1, 4, 1, 3, 3, 1, 1, 2, 2, 2, 2, 3, 1, 1, 1, 2, 1, 4, 1, 3, 3, 1, 1, 2, 2, 2, 2, 3, 1,  
      3, 1, 2, 1, 4, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 3, 1, 2, 1, 4, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 
      1, 1, 2, 1, 4, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 2, 1, 4, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2,
      1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
      1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 
      2, 2, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 
      1, 2, 1, 1, 3, 1, 4, 4, 4, 1, 2, 2, 2, 1, 1, 1, 1, 2, 1, 1, 3, 1, 4, 4, 4, 1, 2, 2, 2, 1, 1, 1, 
      1, 2, 2, 1, 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 1, 1, 1, 2, 2, 1, 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 1, 1, 
      1, 1, 2, 1, 4, 1, 3, 3, 1, 1, 2, 2, 2, 2, 3, 1, 1, 1, 2, 1, 4, 1, 3, 3, 1, 1, 2, 2, 2, 2, 3, 1,  
      3, 1, 2, 1, 4, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 3, 1, 2, 1, 4, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 
      1, 1, 2, 1, 4, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 2, 1, 4, 3, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 
    };

  this->map.load("tileset.png", sf::Vector2u(tileSize, tileSize), level, levelWidth, levelHeight);
  this->miniMap.load("tileset.png", sf::Vector2u(tileSize, tileSize), level, levelWidth, levelHeight, sf::Color(255, 255, 255, 196));

  // player
  player.shape.setRadius(tileSize / 2);
  player.shape.setFillColor(sf::Color::Green);
  player.x = tileSize * levelWidth / 2;
  player.y = tileSize * levelHeight / 2;
  player.shape.setPosition(player.x, player.y);

  // views
  this->view.setSize(tileSize * 20, tileSize * 15);
  this->view.setCenter(tileSize * levelWidth / 2, tileSize * levelHeight / 2);
  this->miniMapView.setSize(tileSize * levelWidth, tileSize * levelHeight);
  this->miniMapView.setCenter(tileSize * levelWidth / 2, tileSize * levelHeight / 2);
  this->miniMapView.setViewport(sf::FloatRect(0.75f, 0, 0.25f, 0.25f));
  
  font.loadFromFile("Warenhaus-Standard.ttf");
  debugText.setFont(font);
  debugText.setCharacterSize(240);
  debugText.setFillColor(sf::Color::Red);
  debugText.setPosition(0, 0);
}

void Game::pushState(std::shared_ptr<GameState> state)
{
  this->states.push(state);
  return;
}

void Game::popState()
{
  this->states.pop();
  return;
}

void Game::changeState(std::shared_ptr<GameState> state)
{
  if(!this->states.empty())
    popState();
  pushState(state);
  return;
}

std::shared_ptr<GameState> Game::peekState()
{
  if(this->states.empty()) return nullptr;
  return this->states.top();
}

void Game::gameLoop()
{
  sf::Clock clock;

  while(this->window.isOpen())
    {
      sf::Time elapsed = clock.restart();
      float dt = elapsed.asSeconds();

      if(peekState() == nullptr) continue;
      peekState()->update(dt);
      this->window.clear(sf::Color::Black);
      peekState()->draw(dt);
      this->window.display();
    }
}

Game::Game()
{
  this->loadSettings();
}

Game::~Game()
{
  while(!this->states.empty()) popState();
}
