#include "GameStateExiting.hpp"

void GameStateExiting::update(const float dt) {
      this->game->window.close();
}

void GameStateExiting::draw(const float dt){

}

GameStateExiting::GameStateExiting(Game* game)
{
  // game state manager
  this->game = game;
}
