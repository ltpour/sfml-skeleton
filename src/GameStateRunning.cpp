#include "GameStateRunning.hpp"
#include "GameStateStartMenu.hpp"
#include "GameStateExiting.hpp"
#include "Player.hpp"

bool GameStateRunning::playerHorizontallyWithinTile() {
  //return true;
  return fmod(this->game->player.getPosition().x, this->game->tileSize) <= 3 | this->game->tileSize - fmod(this->game->player.getPosition().x, this->game->tileSize) <= 3;
}

bool GameStateRunning::playerVerticallyWithinTile() {
  //return true;
  return fmod(this->game->player.getPosition().y, this->game->tileSize) <= 3 | this->game->tileSize - fmod(this->game->player.getPosition().y, this->game->tileSize) <= 3;
}

void GameStateRunning::move(const float dt) {

  // assign direction
  if (this->game->player.direction == NORTH) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
      this->game->player.direction = SOUTH;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
      this->game->player.direction = NORTH;
    else if (this->playerVerticallyWithinTile()) {
      this->game->player.direction = STILL;
      this->game->player.lockVerticallyToGrid(this->game->tileSize);
    }
  }
  else if (this->game->player.direction == SOUTH) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
      this->game->player.direction = NORTH;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
      this->game->player.direction = SOUTH;
    else if (this->playerVerticallyWithinTile()) {
      this->game->player.direction = STILL;
      this->game->player.lockVerticallyToGrid(this->game->tileSize);
    }
  }
  else if (this->game->player.direction == WEST) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
      this->game->player.direction = EAST;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
      this->game->player.direction = WEST;
    else if (this->playerHorizontallyWithinTile()) {
      this->game->player.direction = STILL;
      this->game->player.lockHorizontallyToGrid(this->game->tileSize);
    }
  }
  else if (this->game->player.direction == EAST) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
      this->game->player.direction = WEST;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
      this->game->player.direction = EAST;
    else if (this->playerHorizontallyWithinTile()) {
      this->game->player.direction = STILL;
      this->game->player.lockHorizontallyToGrid(this->game->tileSize);
    }
  }
  else if (this->game->player.direction == STILL) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
      this->game->player.direction = NORTH;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
      this->game->player.direction = SOUTH;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
      this->game->player.direction = WEST;
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
      this->game->player.direction = EAST;
  }

  // move
  if (this->game->player.direction == WEST) {
    this->game->player.move(-200 * dt, 0);
    if (this->game->player.getPosition().x < this->game->tileSize)
      this->game->player.setPosition(this->game->tileSize, this->game->player.getPosition().y);

    if (this->game->level[(int)((floor(this->game->player.getPosition().x) - (int)floor(this->game->player.getPosition().x) % this->game->tileSize) / this->game->tileSize) + this->game->levelWidth * (int)((floor(this->game->player.getPosition().y) - (int)floor(this->game->player.getPosition().y) % this->game->tileSize) / this->game->tileSize)] == 3)
      this->game->player.move(this->game->tileSize - fmod(this->game->player.getPosition().x, this->game->tileSize), 0);
    else {
      if (this->game->view.getCenter().x > this->game->player.getPosition().x)
	this->game->view.move(-200 * dt, 0);
      if (this->game->view.getCenter().x < this->game->view.getSize().x / 2)
	this->game->view.setCenter(this->game->view.getSize().x / 2, this->game->view.getCenter().y);
    }	
  }
  
  if (this->game->player.direction == EAST) {
    this->game->player.move(200 * dt, 0);
    if (this->game->player.getPosition().x > this->game->levelWidth * this->game->tileSize - this->game->tileSize * 2)
      this->game->player.setPosition(this->game->levelWidth * this->game->tileSize - this->game->tileSize * 2, this->game->player.getPosition().y);

    if (this->game->level[(int)((floor(this->game->player.getPosition().x) - (int)floor(this->game->player.getPosition().x) % this->game->tileSize) / this->game->tileSize) + 1 + this->game->levelWidth * (int)((floor(this->game->player.getPosition().y) - (int)floor(this->game->player.getPosition().y) % this->game->tileSize) / this->game->tileSize)] == 3)
      this->game->player.move(-1 * fmod(this->game->player.getPosition().x, this->game->tileSize), 0);
    else {
      if (this->game->view.getCenter().x < this->game->player.getPosition().x)
	this->game->view.move(200 * dt, 0);
      if (this->game->view.getCenter().x > this->game->levelWidth * this->game->tileSize - this->game->view.getSize().x / 2)
	this->game->view.setCenter(this->game->levelWidth * this->game->tileSize - this->game->view.getSize().x / 2, this->game->view.getCenter().y);
    }
  }
  
  if (this->game->player.direction == NORTH) {
    this->game->player.move(0, -200 * dt);
    if (this->game->player.getPosition().y < this->game->tileSize)
      this->game->player.setPosition(this->game->player.getPosition().x, this->game->tileSize);

    if (this->game->level[(int)((floor(this->game->player.getPosition().x) - (int)floor(this->game->player.getPosition().x) % this->game->tileSize) / this->game->tileSize) + this->game->levelWidth * (int)((floor(this->game->player.getPosition().y) - (int)floor(this->game->player.getPosition().y) % this->game->tileSize) / this->game->tileSize)] == 3)
      this->game->player.move(0, this->game->tileSize - fmod(this->game->player.getPosition().y, this->game->tileSize));
    else {
      if (this->game->view.getCenter().y > this->game->player.getPosition().y)
	this->game->view.move(0, -200 * dt);
      if (this->game->view.getCenter().y < this->game->view.getSize().y / 2)
	this->game->view.setCenter(this->game->view.getCenter().x, this->game->view.getSize().y / 2);
    }
  }
  
  if (this->game->player.direction == SOUTH) {
    this->game->player.move(0, 200 * dt);
    if (this->game->player.getPosition().y > this->game->levelHeight * this->game->tileSize -this->game->tileSize * 2)
      this->game->player.setPosition(this->game->player.getPosition().x, this->game->levelHeight * this->game->tileSize -this->game->tileSize * 2);
    
    if (this->game->level[(int)((floor(this->game->player.getPosition().x) - (int)floor(this->game->player.getPosition().x) % this->game->tileSize) / this->game->tileSize) + this->game->levelWidth * (int)((floor(this->game->player.getPosition().y) - (int)floor(this->game->player.getPosition().y) % this->game->tileSize) / this->game->tileSize + 1)] == 3)
      this->game->player.move(0, -1 * fmod(this->game->player.getPosition().y, this->game->tileSize));
    else {
      if (this->game->view.getCenter().y < this->game->player.getPosition().y)
	this->game->view.move(0, 200 * dt);
      if (this->game->view.getCenter().y > this->game->levelHeight * this->game->tileSize - this->game->view.getSize().y / 2)
	this->game->view.setCenter(this->game->view.getCenter().x, this->game->levelHeight * this->game->tileSize - this->game->view.getSize().y / 2);
    }
  }
}

void GameStateRunning::update(const float dt) {
  // move to exit screen
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
      this->game->pushState(std::make_shared<GameStateExiting>(this->game));
    }

  // move back to start screen
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
    {
      this->game->pushState(std::make_shared<GameStateStartMenu>(this->game));
    }

  sf::Event event;
  while (this->game->window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
	this->game->window.close();
    }

  move(dt);

  // set the string to display
  this->game->debugText.setString(std::to_string(this->game->player.getPosition().y));
}

void GameStateRunning::draw(const float dt){
  this->game->window.setView(this->game->view);
  this->game->window.clear();
  this->game->window.draw(this->game->map);
  this->game->window.draw(this->game->player.shape);  
  this->game->window.setView(this->game->miniMapView);
  this->game->window.draw(this->game->miniMap);
  this->game->window.draw(this->game->player.shape);
  this->game->window.draw(this->game->debugText);
  this->game->window.display();
}

GameStateRunning::GameStateRunning(Game* game)
{
  // game state manager
  this->game = game;
}
