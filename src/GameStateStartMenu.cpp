#include "GameStateStartMenu.hpp"
#include "GameStateRunning.hpp"
#include "GameStateExiting.hpp"

void GameStateStartMenu::update(const float dt) {
  // move to exit screen
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
      this->game->pushState(std::make_shared<GameStateExiting>(this->game));
    }

  // start new game
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
      this->game->pushState(std::make_shared<GameStateRunning>(this->game));
    }

  sf::Event event;
  while (this->game->window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
	this->game->window.close();
    }
}

void GameStateStartMenu::draw(const float dt){
  this->game->window.setView(this->game->view);
  this->game->window.clear();
  this->game->window.draw(this->game->map);
  this->game->window.draw(this->game->player.shape);  
  this->game->window.setView(this->game->miniMapView);
  this->game->window.draw(this->game->miniMap);
  this->game->window.draw(this->game->player.shape);  
  this->game->window.display();
}

GameStateStartMenu::GameStateStartMenu(Game* game)
{
  // game state manager
  this->game = game;
}
