#include <SFML/Graphics.hpp>

#include "Player.hpp"

void Player::move(float offsetX, float offsetY)
{
  this->shape.move(offsetX, offsetY);
}

const sf::Vector2f& Player::getPosition () const
{
  this->shape.getPosition();
}

void Player::setPosition (float x, float y)
{
  this->shape.setPosition(x, y);
}

void Player::lockHorizontallyToGrid (int tileSize)
{
  if (fmod(this->getPosition().x, tileSize) <= 3)
    this->setPosition(round(this->getPosition().x / tileSize) * tileSize, this->getPosition().y);
  else if (tileSize - fmod(this->getPosition().x, tileSize) <= 3)
    this->setPosition(round(this->getPosition().x / tileSize) * tileSize, this->getPosition().y);
}

void Player::lockVerticallyToGrid (int tileSize)
{
  if (fmod(this->getPosition().y, tileSize) <= 3)
    this->setPosition(this->getPosition().x, round(this->getPosition().y / tileSize) * tileSize);
  else if (tileSize - fmod(this->getPosition().y, tileSize) <= 3)
    this->setPosition(this->getPosition().x, round(this->getPosition().y / tileSize) * tileSize);
}

Player::Player()
{
  direction = STILL;
}

Player::~Player()
{
  
}
