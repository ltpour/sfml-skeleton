#include <SFML/Graphics.hpp>

#include "Game.hpp"
#include "GameStateStartMenu.hpp"

int main()
{
  // game loop
  Game game;
  game.pushState(std::make_shared<GameStateStartMenu>(&game));
  game.gameLoop();

  return 0;
}
